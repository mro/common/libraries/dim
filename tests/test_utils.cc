
#include <boost/thread.hpp>
#include <iostream>

#include <spawn.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "test_utils.hh"

void DimInfoWaiter::reset()
{
    boost::unique_lock<boost::mutex> lock(m_lock);
    m_count = 0;
}

bool DimInfoWaiter::waitUpdate(int count, unsigned int ms)
{
    boost::unique_lock<boost::mutex> lock(m_lock);

    while (m_count < count)
    {
        if (m_cond.wait_for(lock,
                boost::chrono::milliseconds(ms)) == boost::cv_status::timeout)
            break;
    }
    return m_count >= count;
}

void DimInfoWaiter::infoHandler()
{
    DimInfo::infoHandler();
    {
        boost::unique_lock<boost::mutex> lock(m_lock);
        ++m_count;
    }
    m_cond.notify_all();
}

pid_t aspawn(const char * const argv[])
{
    pid_t pid;
    posix_spawn_file_actions_t actions;

    if (posix_spawn_file_actions_init(&actions) != 0)
        return -1;
    posix_spawn_file_actions_addclose(&actions, STDIN_FILENO);


    if (posix_spawn(&pid, argv[0], &actions, NULL,
                    (char * const *)(argv), environ) != 0) {
        pid = -1;
    }
    posix_spawn_file_actions_destroy(&actions);
    return pid;
}

void terminate(pid_t pid)
{
    int stat_loc;
    if (pid < 0)
        return;

    kill(pid, SIGTERM);
    boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
    if (waitpid(pid, &stat_loc, WNOHANG) == -1)
    {
        kill(pid, SIGKILL);
        waitpid(pid, &stat_loc, 0);
    }
}
