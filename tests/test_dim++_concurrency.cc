

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#include <boost/thread.hpp>
#include <boost/atomic.hpp>
#include <boost/chrono.hpp>
#include <string>
#include <stdlib.h>

#include "dim.hxx"
#include "dis.hxx"
#include "dic.hxx"

#include "config.h"

/**
 * @brief test case service
 * @details creates a "TEST" service and loop update it in a thread.
 */
class TestConcurrencyService
{
public:
    TestConcurrencyService() :
        m_serv("TEST", (char *) ""),
        m_running(false)
    {}

    ~TestConcurrencyService()
    {
        stop();
    }

    void start()
    {
        m_running = true;
        m_thread = new boost::thread(boost::bind(&TestConcurrencyService::run, this));
    }

    void stop() {
        if (m_thread) {
            m_running = false;
            m_thread->join();
            delete m_thread;
            m_thread = 0;
        }
    }

    inline unsigned int loops() const { return m_loops; }

protected:
    void run()
    {
        std::string str1 = "";
        std::string str2 = "test string";

        for (m_loops = 0; m_running; ++m_loops)
        {
            if (m_loops & 0x01)
                m_serv.updateService(&str1[0]);
            else
                m_serv.updateService(&str2[0]);
        }
    }

    DimService m_serv;
    boost::thread *m_thread;
    boost::atomic<bool> m_running;
    unsigned int m_loops;
};

class TestConcurrency : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestConcurrency);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    pid_t m_dns;
    boost::thread *m_thread;
    boost::atomic<bool> m_running;
    DimService *m_serv;

public:
    void setUp()
    {
        setenv("DIM_DNS_PORT", "12345", 1);
        setenv()"DIM_DNS_NODE", "127.0.0.1", 1);

        m_thread = 0;

        m_dns = spawn(BUILDDIR "/src/dns", "-d");
        CPPUNIT_ASSERT(m_dns >= 0);
        boost::this_thread::sleep_for(boost::chrono::seconds(3));
    }

    void tearDown()
    {
        terminate(m_dns);
        m_dns = -1;

        DimServer::stop();
    }

    /**
     * @details this test shows the concurrent access issue that can happen in
     * DIM C++ wrapper when updateService is being called whilst a network call
     * is being serviced.
     *
     * @see https://gitlab.cern.ch/apc/dim/issues/1 for details
     */
    void simple()
    {
        TestConcurrencyService serv;

        serv.start();

        DimServer::start("testServer");
        const int num_loop = 5000000;

        for (int i = 0; i < num_loop; ++i)
        {
            DimCurrentInfo info("TEST", (char *) "nope");

            std::string test = info.getString();
            if ((i % 50000) == 0)
            {
                std::cout << "loop: " << (i * 100 / num_loop) <<
                    "% value: \"" << test << "\"" << std::endl;
            }
            if (test != "" && test != "test string")
            {
                std::cout << "invalid content: " << test
                    << " size: " << info.getSize();
                CPPUNIT_FAIL("invalid content");
            }
        }

        serv.stop();
        std::cout << "value was updated " << serv.loops() << " times "
            << " and retrieved " << num_loop << " times" << std::endl;
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestConcurrency);
