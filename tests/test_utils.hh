

#ifndef __TEST_UTILS_HPP__
#define __TEST_UTILS_HPP__

#include <boost/thread.hpp>
#include <sys/types.h>
#include "dic.hxx"

class DimInfoWaiter : public DimInfo
{
public:
    template<typename T>
    DimInfoWaiter(const char *name, T t);

    void reset();
    bool waitUpdate(int count = 1, unsigned int ms = 1000);

    virtual void infoHandler();

protected:
    unsigned int m_count;
    boost::condition_variable m_cond;
    boost::mutex m_lock;
};

template <typename T>
DimInfoWaiter::DimInfoWaiter(const char *name, T t) :
    DimInfo(name, t),
    m_count(0)
{}

pid_t aspawn(const char * const argv[]);

template<typename... Arg>
pid_t spawn(const char *file, Arg... args)
{
    const char * const list[] = { file, args..., 0 };
    return aspawn(list);
}

void terminate(pid_t pid);

#endif
