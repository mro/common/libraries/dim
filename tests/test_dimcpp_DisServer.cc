
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#include <boost/thread.hpp>
#include <boost/atomic.hpp>
#include <boost/chrono.hpp>
#include <string>

#include "dim.hxx"
#include "dis.hxx"
#include "dic.hxx"

#include "config.h"
#include "test_utils.hh"

#define EQ CPPUNIT_ASSERT_EQUAL

extern "C" {
int dis_no_dns();
}

class TestDimServer : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDimServer);
    CPPUNIT_TEST(start_stop);
    CPPUNIT_TEST_SUITE_END();

    pid_t m_dns;

public:
    void setUp()
    {
        m_dns = spawn(BUILDDIR "/src/dns", "-d");
        CPPUNIT_ASSERT(m_dns >= 0);
        boost::this_thread::sleep_for(boost::chrono::seconds(3));
    }

    void tearDown()
    {
        terminate(m_dns);
        m_dns = -1;

        DimServer::stop();
    }

    void start_stop()
    {
        /*
            DimServer dns connection was not properly cleaned-up when
            no services have been registered

            It has some very bad side-effects such as not registering
            new services once started back
         */
        DimServer::start("testServer");
        EQ(0, dis_no_dns());

        // uncomment this to make it work without the fix in dis.c
        // DimService service("sample", 1);

        DimServer::stop();
        EQ(1, dis_no_dns()); // A dis->dns connection remains
    }

};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDimServer);
